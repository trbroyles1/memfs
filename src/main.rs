mod memfs;
use clap::{App, Arg};
use fuser::{mount2, MountOption};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

use crate::memfs::MemFs;

fn main() {
	let args = App::new("MemFS")
		.version("0.1.0")
		.author("Ryan Broyles <me@trbroyles.com>")
		.about("Learning about FUSE filesystems")
		.arg(Arg::with_name("path").takes_value(true).value_name("[PATH]").help("The path at which to mount").required(true))
		.get_matches();

	std::env::set_var("RUST_LOG", "Info");
	pretty_env_logger::init_timed();

	let path = args.value_of("path").unwrap();
	let mount_opts: [MountOption; 3] = [MountOption::FSName("MemFS".to_string()), MountOption::DefaultPermissions, MountOption::Sync];

	let fs = MemFs::new();

	trace!("Mounting at {}", &path);
	match mount2(fs, &path, &mount_opts) {
		Ok(_) => {
			trace!("Unmounted cleanly.")
		}
		Err(e) => error!("Err at end: {}", e.to_string()),
	}
}
