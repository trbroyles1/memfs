use std::borrow::BorrowMut;
use std::collections::HashMap;
use std::ffi::{OsStr, OsString};
use std::mem;
use std::os::unix::prelude::OsStrExt;
use std::time::{Duration, SystemTime};

use fuser::{FileAttr, FileType, Filesystem, Request};
use libc::{c_int, EEXIST, ENODATA, ENOENT, ENOSYS, ENOTEMPTY, ERANGE};
use log::trace;

const KB: usize = 1024;
#[allow(dead_code)]
const MB: usize = 1000 * KB;
#[allow(dead_code)]
const GB: usize = 1000 * MB;

const ALLOC_SIZE: usize = 4 * KB; //BS=1M

/*
	MemFS is the struct that implements the Filesystem trait.
	'nodes' is a Vec of MemFS node. Each entry in the Vec is an inode in the FS. The index of each MemFsNode in the Vec becomes the inode number in the FS.
	The first item in the vec is a dummy entry that will never be used anywhere, because Vecs are 0-indexed but inodes are 1-indexed (0 indicates a null inode).
	The root of the FS is the 2nd item in the Vec; inode 1

	'dead_nodes' is a Vec of inode numbers (err, nodes vec indexes...) of deleted items that can be overwritten.

	The two biggest points to recall for understanding this implementation of FUSE are:
	1. One FS inode (one entry in the filesystem, either directory or file) == one MemFsNode in MemFs's 'nodes' Vec
	2. inode number (often passed to a function call as '_ino') == index of the inode (aka the MemFsNode) in MemFs's 'nodes' Vec
*/
pub struct MemFs {
	nodes:      Vec<MemFsNode>,
	dead_nodes: Vec<usize>,
}

#[derive(PartialEq)]

//MemFsNode represents one inode in the FS
struct MemFsNode {
	//name is an OsString containing the file / directory name.
	name:     OsString,
	//attrs is the FileAttr struct for the node
	attrs:    FileAttr,
	//xattrs is a HashMap of OsString:u8 xattrs for the node.
	xattrs:   HashMap<OsString, Vec<u8>>,
	//parent is the inode of the parent [directory] of this MemFsNode. It's an option because Root doesn't have a parent.
	parent:   usize,
	//children is a Vec of all of the inodes that are children of this MemFsNode
	children: Vec<usize>,
	//lookup is a HashMap mapping the names of all children of this MemFsNode to their inode
	lookup:   HashMap<OsString, usize>,
	//data contains the file data, for file nodes (attrs.kind == FileType::RegularFile). It's an option because dirs don't have data.
	data:     Option<Vec<u8>>,
}

//This impl of MemFs is only here to provide a "new" method for instantiating MemFs
impl MemFs {
	pub fn new() -> MemFs {
		let mut fs = MemFs {
			nodes:      vec![MemFsNode {
				//This node is the "dummy" index-0 entry in the vec
				name:     OsString::from("dummy_node"),
				parent:   0,
				children: vec![1],
				lookup:   HashMap::new(),
				data:     None,
				attrs:    FileAttr {
					ino:     0,
					size:    0,
					blocks:  0,
					atime:   SystemTime::now(),
					mtime:   SystemTime::now(),
					ctime:   SystemTime::now(),
					crtime:  SystemTime::now(),
					kind:    fuser::FileType::Directory,
					perm:    511,
					nlink:   0,
					uid:     0,
					gid:     0,
					rdev:    0,
					blksize: 0,
					flags:   0,
				},
				xattrs:   HashMap::new(),
			}],
			dead_nodes: vec![],
		};
		let root = MemFsNode {
			//This node is the root of the filesystem
			name:     OsString::from("MemFSRoot"),
			parent:   0,
			children: vec![],
			lookup:   HashMap::new(),
			data:     None, //per note above, the root of the FS is a directory and has no data... derp derp.
			attrs:    FileAttr {
				ino:     1,
				size:    0,
				blocks:  0,
				atime:   SystemTime::now(),
				mtime:   SystemTime::now(),
				ctime:   SystemTime::now(),
				crtime:  SystemTime::now(),
				kind:    fuser::FileType::Directory,
				perm:    511,
				nlink:   0,
				uid:     0,
				gid:     0,
				rdev:    0,
				blksize: 0,
				flags:   0,
			},
			xattrs:   HashMap::new(),
		};

		fs.nodes.push(root);

		fs
	}
}

impl<'a> Filesystem for MemFs {
	/*
		init is called when the FS is first mounted, can be used to do general setup.
	*/
	fn init(&mut self, _req: &Request<'_>, _config: &mut fuser::KernelConfig) -> Result<(), c_int> {
		Ok(())
	}

	fn destroy(&mut self) {
	}

	/*
		lookup gets called to validate names (of files, directories...) and translate them to inodes.
		This can be triggered by anything that involves referring to a file by its name.
		e.g. opening, reading, or deleting it. The Fish shell also calls it a lot, to provide its autocomplete suggestions.
		calling lookup is rarely an end unto itself; typically it is followed by a call to open, write, etc to do something
		else with the file by inode.
	*/
	fn lookup(&mut self, _req: &Request<'_>, _parent: u64, _name: &std::ffi::OsStr, reply: fuser::ReplyEntry) {
		trace!("lookup: parent {}, name {}", _parent, _name.to_str().unwrap());
		let parent_inode_idx = _parent as usize;
		let sought_inode_name = _name;

		//sanity check that the inode passed to us actually exists
		if parent_inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//get a reference to the parent node by inode index
		let parent_node = &self.nodes[parent_inode_idx];

		//check its lookup index for the sought inode by name
		if let Some(sought_node_idx) = parent_node.lookup.get(sought_inode_name) {
			//if found, return its attrs
			let sought_node = &self.nodes[*sought_node_idx];
			reply.entry(&Duration::ZERO, &sought_node.attrs, 0);
		} else {
			//otherwise reply with the "doesn't exist" error code.
			reply.error(ENOENT);
		}
	}

	fn forget(&mut self, _req: &Request<'_>, _ino: u64, _nlookup: u64) {
	}

	//getattr is called to look up the attrs of a given inode by inode number.
	fn getattr(&mut self, _req: &Request<'_>, _ino: u64, reply: fuser::ReplyAttr) {
		trace!("getattr: ino {}", _ino);
		let inode_idx = _ino as usize;

		//sanity check that the inode passed to us actually exists
		if inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//return its attributes
		reply.attr(&Duration::ZERO, &self.nodes[inode_idx].attrs);
	}

	/*
		setattr is called to set the attrs of a given inode by inode number.
		The only time I have seen this called thusfar is just after a file's creation.
		but, we could also imagine things like touching the file, chowning it, chmoding it etc would also do this.
	*/
	fn setattr(
		&mut self, _req: &Request<'_>, _ino: u64, _mode: Option<u32>, _uid: Option<u32>, _gid: Option<u32>, _size: Option<u64>, _atime: Option<fuser::TimeOrNow>,
		_mtime: Option<fuser::TimeOrNow>, _ctime: Option<std::time::SystemTime>, _fh: Option<u64>, _crtime: Option<std::time::SystemTime>,
		_chgtime: Option<std::time::SystemTime>, _bkuptime: Option<std::time::SystemTime>, _flags: Option<u32>, reply: fuser::ReplyAttr,
	) {
		trace!("setattr: ino {}", _ino);
		let inode_idx = _ino as usize;

		//sanity check that the inode passed to us actually exists
		if inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//get a [mut] reference to the attrs of the target inode by inode index
		let mut inode_attrs = self.nodes[inode_idx].attrs.borrow_mut();

		//now go through and copy over the attrs given to us. Most of them are wrapped in Option, because we might not be setting
		//all of them.
		if let Some(uid) = _uid {
			inode_attrs.uid = uid
		}
		if let Some(gid) = _gid {
			inode_attrs.gid = gid
		}
		if let Some(size) = _size {
			inode_attrs.size = size
		}
		if let Some(atime) = _atime {
			match atime {
				fuser::TimeOrNow::SpecificTime(t) => {
					inode_attrs.atime = t;
				}
				fuser::TimeOrNow::Now => {
					inode_attrs.atime = SystemTime::now();
				}
			}
		}

		if let Some(mtime) = _mtime {
			match mtime {
				fuser::TimeOrNow::SpecificTime(t) => {
					inode_attrs.mtime = t;
				}
				fuser::TimeOrNow::Now => {
					inode_attrs.mtime = SystemTime::now();
				}
			}
		}

		if let Some(ctime) = _ctime {
			inode_attrs.ctime = ctime;
		}

		if let Some(flags) = _flags {
			inode_attrs.flags = flags
		}

		reply.attr(&Duration::ZERO, &inode_attrs);
	}

	fn readlink(&mut self, _req: &Request<'_>, _ino: u64, reply: fuser::ReplyData) {
		reply.error(ENOSYS);
	}

	//Called to create a new file.
	fn mknod(&mut self, _req: &Request<'_>, _parent: u64, _name: &std::ffi::OsStr, _mode: u32, _umask: u32, _rdev: u32, reply: fuser::ReplyEntry) {
		trace!("mknod: parent {}", _parent);
		let parent_inode_idx = _parent as usize;

		//sanity check that the parent inode passed to us actually exists
		if parent_inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//sanity check that the given file name doesn't already exist.
		if self.nodes[parent_inode_idx].lookup.contains_key(_name) {
			reply.error(EEXIST);
			return;
		}

		//creation / modification / access time are now
		let time = SystemTime::now();

		/*
			So here's the rub on making the new node: this is made sticky by the fact that we also have to do this thing called deleting nodes when files/dirs get rmed.
			The first thought might be "well, when we delete it lets take it out of our vec of nodes." Problem... the indexes of that vec -- which, we recall, are our
			inode numbers -- are also used by nodes to maintain lists of their children, as well as their name-to-inode lookup tables.
			If we remove the nodes from the vec and stuff shifts to the left, all our children lists and lookup tables are all out of whack. so we don't, instead we put the number
			of the removed node on a list of dead nodes that we can reuse later for file/folder creation.

			The reason why this is fun when we get to file/folder creation is that we need to either a) add a completely new MemFsNode -- if there is nothing on the deleted ("dead") list --
			or we need to reuse an existing one if such exists. Now, we'd like to do this without copying these big structs around all over the place.

			So what we do below, is declare a new_node [mut] reference to a MemFsNode, without initializing it.
				Then, we see if we have a dead node to reuse.
				If so, we take it off the front of the list, and point our new_node and our new_inode_index at it.

				But if we have nothing to reuse, we need to make a new one. So, we make a new index -- one we're going to push onto the list -- and we drop to unsafe to zero-initialize a
				MemFsNode.

				However, the zero-initialization is not actually unsafe, because in either case we immediately proceed to fill in all of the properties of the MemFsNode.
				So, that's what's up with that.
		*/
		let new_inode_index: usize;
		let new_node: &mut MemFsNode;

		if self.dead_nodes.len() > 0 {
			new_inode_index = self.dead_nodes.remove(0);
			new_node = &mut self.nodes[new_inode_index];
		} else {
			new_inode_index = self.nodes.len();
			/*
				So how this works is:
				1. zero-initialize memory the size of a MemFsNode using a dummy type ( [u8])
				2. transmute it into a MemFsNode
				3. Push it onto the end of the list of nodes
				4. Set our new_node mut ref to point to it

				The reason we have to do the dummy-type-and-transmute thing is because we can't directly zero-initalize a MemFsNode
				And it's not actually unsafe to do. See discussion above.
				In particular, note that we proceed to immediately set all attributes
				of the MemFsNode. So it isn't unsafe.
			*/
			let new_node_init: MemFsNode = unsafe { mem::transmute(mem::zeroed::<[u8; mem::size_of::<MemFsNode>()]>()) };
			self.nodes.push(new_node_init);
			new_node = self.nodes.last_mut().unwrap();
		}

		new_node.name = _name.to_os_string();
		new_node.parent = parent_inode_idx;
		/*
			yeah yeah, files are never gonna have children.
			The other option was to put children in an Option, and calling .as_mut().unwrap() every where gets tedious.
			It is what it is.
		*/
		new_node.children = vec![];
		new_node.lookup = HashMap::new(); // see above comment about children and apply to lookup.
		new_node.data = Some(vec![]);
		new_node.attrs = FileAttr {
			ino:     new_inode_index as u64,
			size:    0,
			blocks:  0,
			atime:   time,
			mtime:   time,
			ctime:   time,
			crtime:  time,
			kind:    FileType::RegularFile,
			perm:    511,
			nlink:   0,
			uid:     _req.uid(),
			gid:     _req.gid(),
			rdev:    0,
			blksize: ALLOC_SIZE as u32,
			flags:   0,
		};
		new_node.xattrs = HashMap::new();

		//update the parent's list of children and name lookup table to include the new node
		let parent_node = &mut self.nodes[parent_inode_idx];
		parent_node.children.push(new_inode_index);
		parent_node.lookup.insert(_name.to_os_string(), new_inode_index);

		//reply with the attrs of the new node.
		reply.entry(&Duration::ZERO, &self.nodes[new_inode_index].attrs, 0);
	}

	/*
		mkdir is called to... make... directories. derp derp.
	*/
	fn mkdir(&mut self, _req: &Request<'_>, _parent: u64, _name: &std::ffi::OsStr, _mode: u32, _umask: u32, reply: fuser::ReplyEntry) {
		//this is going to look real similar to making a file... perhaps we can refactor this later.
		trace!("mkdir: parent {}", _parent);
		let parent_inode_idx = _parent as usize;

		//sanity check that the parent inode passed to us actually exists
		if parent_inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//sanity check that the given directory name doesn't already exist.
		if self.nodes[parent_inode_idx].lookup.contains_key(_name) {
			reply.error(EEXIST);
			return;
		}

		//creation / modification / access time are now
		let time = SystemTime::now();

		//see discussion in mknod about what happens here.
		//in particular, see it for the note about unsafe usage.

		let new_inode_index: usize;
		let new_node: &mut MemFsNode;

		if self.dead_nodes.len() > 0 {
			new_inode_index = self.dead_nodes.remove(0);
			new_node = &mut self.nodes[new_inode_index];
		} else {
			new_inode_index = self.nodes.len();
			//see discussion in mknod about what is happening here, and unsafe usage.
			let new_node_init: MemFsNode = unsafe { std::mem::transmute(std::mem::zeroed::<[u8; std::mem::size_of::<MemFsNode>()]>()) };
			self.nodes.push(new_node_init);
			new_node = self.nodes.last_mut().unwrap();
		}

		new_node.name = _name.to_os_string();
		new_node.parent = parent_inode_idx;

		new_node.children = vec![];
		new_node.lookup = HashMap::new(); // see above comment about children and apply to lookup.
		new_node.data = Some(vec![]);
		new_node.attrs = FileAttr {
			ino:     new_inode_index as u64,
			size:    0,
			blocks:  0,
			atime:   time,
			mtime:   time,
			ctime:   time,
			crtime:  time,
			kind:    FileType::Directory,
			perm:    511,
			nlink:   0,
			uid:     _req.uid(),
			gid:     _req.gid(),
			rdev:    0,
			blksize: 0,
			flags:   0,
		};
		new_node.xattrs = HashMap::new();

		//update the parent's list of children and name lookup table to include the new node
		let parent_node = &mut self.nodes[parent_inode_idx];
		parent_node.children.push(new_inode_index);
		parent_node.lookup.insert(_name.to_os_string(), new_inode_index);

		//reply with the attrs of the new node
		reply.entry(&Duration::ZERO, &self.nodes[new_inode_index].attrs, 0);
	}

	/*
		Unlink gets called to delete files (a la rm).
		Probably elsewhere also, but deleting files is what I've seen so far
	*/
	fn unlink(&mut self, _req: &Request<'_>, _parent: u64, _name: &std::ffi::OsStr, reply: fuser::ReplyEmpty) {
		/*
			deletion (err, unlinking) is rather involved.
			We get the inode of the parent of what we're deleting (as _parent), and the name of what we're supposed to delete (as _name).
			We need to:
				1. look up the inode index of what we want to delete.
				2. remove that index from the parent's list of child inodes -- noting that the index *of* the inode is *not* its index in the list of *children* of the parent. So that's something to watch for
				3. then we need to remove the name of the inode from the parent's lookup table
				4. then we need to need to remove the inode from the list of inodes
				5. then we need to push the index of that inode onto a list of inode numbers we can reuse later.
		*/

		trace!("unlink: parent {}, name {}", _parent, _name.to_str().unwrap());
		let parent_inode_idx = _parent as usize;

		//sanity check that the parent inode passed to us actually exists
		if parent_inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//get mutable references to a) our node list and b) the parent inode
		let nodes = &mut self.nodes;
		let parent = &mut nodes[parent_inode_idx];

		//find the index / inode number / node id of the node to drop
		let drop_node_id: usize;
		if let Some(node_id) = parent.lookup.get(_name) {
			drop_node_id = *node_id;
		} else {
			reply.error(ENOENT);
			return;
		}

		//remove the node we're dropping from the list of children of the parent
		let children_of_parent = &mut parent.children;
		for child in children_of_parent.iter().enumerate() {
			//note that we remove at whatever index we found the node id value we're dropping
			if *child.1 == drop_node_id {
				children_of_parent.remove(child.0);
				break;
			}
		}

		//purge the name of the node we're dropping from the parent's lookup table
		parent.lookup.remove(_name);

		/*
			NO! because our inode numbers -- which are present in children indexes and lookup tables -- are based on the indexes in the nodes vec,
			we most certainly do NOT want to in fact purge the node from the nodes vec.
			If we did that, everything in the vec would get shifted to the left, and we would need to go through and recalculate the node indexes (err, inode numbers)
			of everything in *all* children arrays and lookup tables in the entire FS.
		*/
		//nodes.remove(drop_node_id as usize);

		/*
			Instead, leave it just where it is, and push its index aka inode number onto our list of dead nodes.
			Subsequent file / directory create operations can then overwrite it.
		*/
		self.dead_nodes.push(drop_node_id);

		reply.ok();
	}

	fn rmdir(&mut self, _req: &Request<'_>, _parent: u64, _name: &std::ffi::OsStr, reply: fuser::ReplyEmpty) {
		trace!("rmdir: parent {}, name {}", _parent, _name.to_str().unwrap());
		let parent_inode_idx = _parent as usize;

		//sanity check that the parent inode passed to us actually exists
		if parent_inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//get mutable references to a) our node list and b) the parent inode
		let nodes = &mut self.nodes;

		//find the index / inode number / node id of the node to drop
		let drop_node_id: usize;
		{
			//scoping to drop mutable borrow of nodes long enough to allow us to check to make sure this node has no children later
			let parent = &mut nodes[parent_inode_idx];

			if let Some(node_id) = parent.lookup.get(_name) {
				drop_node_id = *node_id;
			} else {
				reply.error(ENOENT);
				return;
			}
		}

		//check to be sure there are no files under the node (directory)
		if nodes[drop_node_id].children.len() > 0 {
			reply.error(ENOTEMPTY);
			return;
		}

		// 'parent' var must be re-declared here because it is scoped above to prevent borrow issues
		let parent = &mut nodes[parent_inode_idx];

		//remove the node we're dropping from the list of children of the parent
		let children_of_parent = &mut parent.children;
		for child in children_of_parent.iter().enumerate() {
			//note that we remove at whatever index we found the node id value we're dropping
			if *child.1 == drop_node_id {
				children_of_parent.remove(child.0);
				break;
			}
		}

		//purge the name of the node we're dropping from the parent's lookup table
		parent.lookup.remove(_name);

		/*
			Push its index aka inode number onto our list of dead nodes.
			Subsequent file / directory create operations can then overwrite it.
		*/
		self.dead_nodes.push(drop_node_id);

		reply.ok();
	}

	fn symlink(&mut self, _req: &Request<'_>, _parent: u64, _name: &std::ffi::OsStr, _link: &std::path::Path, reply: fuser::ReplyEntry) {
		reply.error(ENOSYS);
	}

	/*
		rename is called to rename files, this also means if they are moved. which is why we have an old parent and new parent
	*/
	fn rename(&mut self, _req: &Request<'_>, _parent: u64, _name: &std::ffi::OsStr, _newparent: u64, _newname: &std::ffi::OsStr, _flags: u32, reply: fuser::ReplyEmpty) {
		trace!("rename: parent {}, name {}, newparent {}, newname {}", _parent, _name.to_str().unwrap(), _newparent, _newname.to_str().unwrap());
		let old_parent_inode_idx = _parent as usize;
		let new_parent_inode_idx = _newparent as usize;
		let old_name = _name;
		let new_name = _newname;

		//some sanity checks
		if old_parent_inode_idx >= self.nodes.len() || new_parent_inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		if !self.nodes[old_parent_inode_idx].lookup.contains_key(old_name) {
			reply.error(ENOENT);
			return;
		}

		if self.nodes[new_parent_inode_idx].lookup.contains_key(new_name) {
			reply.error(EEXIST);
			return;
		}
		/*
			why the hoop-jumping above with the index access to the parents above
			and with the dedicated scopes for working with the parents below?
			because the old parent and the new parent may very, very well be the same.
			if this is just an in-directory rename of a file, for instance.
		*/
		let renaming_node_idx: usize;
		{
			//remove the renaming node from the old parent's child list and lookup table...
			let old_parent_node = &mut self.nodes[old_parent_inode_idx];

			renaming_node_idx = old_parent_node.lookup.remove(old_name).unwrap();

			let mut old_child_to_remove: usize = 0;
			for child in old_parent_node.children.iter().enumerate() {
				//node that in the tuple, 0 is the index in the children list, 1 is the actual stored child node number
				if *child.1 == renaming_node_idx {
					old_child_to_remove = child.0;
				}
			}

			old_parent_node.children.remove(old_child_to_remove);
		}
		{
			//then add it to the new one...
			let new_parent_node = &mut self.nodes[new_parent_inode_idx];
			new_parent_node.children.push(renaming_node_idx);
			new_parent_node.lookup.insert(new_name.to_os_string(), renaming_node_idx);
		}

		//finally, update the renaming node itself
		let renaming_node = &mut self.nodes[renaming_node_idx];
		renaming_node.name = new_name.to_os_string();
		renaming_node.parent = new_parent_inode_idx;

		reply.ok();
	}

	fn link(&mut self, _req: &Request<'_>, _ino: u64, _newparent: u64, _newname: &std::ffi::OsStr, reply: fuser::ReplyEntry) {
		reply.error(ENOSYS);
	}

	fn open(&mut self, _req: &Request<'_>, _ino: u64, _flags: i32, reply: fuser::ReplyOpen) {
		trace!("open file: ino {} flags {}", _ino, _flags);
		reply.opened(0, _flags as u32);
	}

	/*
		read is called to read the contents of a file
		an offset is passed to us as a starting point to read data from.
		This offset must be taken into account when doing reads
	*/
	fn read(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _offset: i64, _size: u32, _flags: i32, _lock_owner: Option<u64>, reply: fuser::ReplyData) {
		trace!("read file: ino {}, fh {}, offset {}, size {} flags {}", _ino, _fh, _offset, _size, _flags);
		let inode_idx = _ino as usize;

		//sanity check that the inode passed to us actually exists
		if inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//get a [mut] ref to the node
		let node = &mut self.nodes[inode_idx];

		//let's not unwrap directly. What if for some crazy reason, read was called on a dir ?
		match node.data.as_ref() {
			Some(d) => {
				//some convenience vars
				let offset_uz = _offset as usize; //starting offset to read from
				let size_uz = _size as usize; //total bytes to read

				//if the data requested is bigger than our total size, ensure to return only as much as we have
				if offset_uz + size_uz > d.len() {
					//slicing into our data vec<[u8]> to return the starting offset to the end
					reply.data(&d[_offset as usize..]);
					trace!("data: {}", String::from_utf8(d.to_owned()).unwrap());
				} else {
					//if our total size is bigger than what is asked, only give what was asked
					reply.data(&d[offset_uz..offset_uz + size_uz]);
					return;
				}
			}
			//if there is no data vec, send the error code for "doesn't exist"
			None => reply.error(ENOENT),
		}
	}

	/*
		write is called to write data to a file.
		much as with read, we'll be passed an offset from which
		we should start writing.
	*/
	fn write(
		&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _offset: i64, _data: &[u8], _write_flags: u32, _flags: i32, _lock_owner: Option<u64>, reply: fuser::ReplyWrite,
	) {
		trace!("write file: ino {}, fh {}, offset {}, data.len {} flags {}", _ino, _fh, _offset, _data.len(), _flags);
		let inode_idx = _ino as usize;

		//sanity check that the inode passed to us actually exists
		if inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//some convenience vars
		let offset_uz = _offset as usize; //starting offset to write from
		let size_uz = _data.len() as usize; //total bytes to write

		//get a [mut] ref to the node
		let node = &mut self.nodes[inode_idx];

		//it for some reason the data vec didn't exist yet, let's initialize it to the our default block size
		if node.data == None {
			node.data = Some(Vec::with_capacity(ALLOC_SIZE));
		}

		//now let's take a [mut] ref to the data vec for the node
		let d = node.data.as_mut().unwrap();

		//if the offset + len of the data we're being asked to write exceeds the capacity of our vec, reserve another block.
		if offset_uz + size_uz > d.capacity() {
			d.reserve_exact(ALLOC_SIZE);
			trace!("new capacity is {}", d.capacity());
		}

		//if the length of our vec aligns with the offset we're being asked to write from, it means we're appending
		//so we can just extend our vec from the slice we're given
		if d.len() == offset_uz {
			d.extend_from_slice(_data);
		} else {
			//we're not just appending, we're writing on at least a partial overlap with existing data. so some special treatment is needed.
			//now let's resize the vec to hold the data, filling with zeroes
			if d.len() < offset_uz + size_uz {
				d.resize(offset_uz + size_uz, 0);
			}

			//now splice in the bytes from the data slice we're given, on top of the 0 bytes from the
			//resize we just did
			d.splice(offset_uz..offset_uz + size_uz, _data.to_owned());
		}

		//very important! update the size in the node's attrs to be the total size
		//of the data vec.
		node.attrs.size = d.len() as u64;

		/*
			calculate and update how many blocks the data uses
			this is defined as:
					( (C + BS ) / BS ).floor()
			where:
				C       = the capacity of the data vec
				BS      = our default block size
				floor() = the floor value (largest int <= a floating-point number)
		*/
		let num_blocks = ((d.capacity() as f64 + ALLOC_SIZE as f64) / ALLOC_SIZE as f64).floor();
		trace!("new block count is {}", num_blocks);
		node.attrs.blocks = num_blocks as u64;

		//reply with the number of bytes written, and we're done.
		reply.written(_data.len() as u32);
	}

	fn flush(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _lock_owner: u64, reply: fuser::ReplyEmpty) {
		reply.ok();
		//reply.error(ENOSYS);
	}

	fn release(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _flags: i32, _lock_owner: Option<u64>, _flush: bool, reply: fuser::ReplyEmpty) {
		reply.ok();
	}

	fn fsync(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _datasync: bool, reply: fuser::ReplyEmpty) {
		reply.error(ENOSYS);
	}

	fn opendir(&mut self, _req: &Request<'_>, _ino: u64, _flags: i32, reply: fuser::ReplyOpen) {
		reply.opened(0, 0);
	}

	#[allow(unused_must_use)] //for now we don't care that we aren't using the result of reply.add()
	fn readdir(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _offset: i64, mut reply: fuser::ReplyDirectory) {
		trace!("read dir: ino {}, fh {}, offset {}", _ino, _fh, _offset);
		if _offset != 0 {
			reply.error(ENOENT);
			return;
		}

		if None == self.nodes.get(_ino as usize) {
			reply.error(ENOENT);
			return;
		}

		let node = self.nodes.get(_ino as usize).unwrap();

		reply.add(1, 0, fuser::FileType::Directory, OsStr::new("."));
		reply.add(1, 1, fuser::FileType::Directory, OsStr::new(".."));

		for (idx, child) in node.children[_offset as usize..].iter().enumerate() {
			let cnode = &self.nodes[*child as usize];
			let true_offset = idx as i64 + _offset + 2;

			if reply.add(cnode.attrs.ino, true_offset, cnode.attrs.kind, &cnode.name) {
				reply.ok();
				return;
			}
		}

		reply.ok();

		/*
		if _ino == 1 && _offset == 0 {
			reply.add(1, 0, fuser::FileType::Directory, OsStr::new("."));
			reply.add(1, 1, fuser::FileType::Directory, OsStr::new(".."));
			reply.ok();
		} else {
			reply.error(ENOENT);
		}
		*/
	}

	fn readdirplus(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _offset: i64, reply: fuser::ReplyDirectoryPlus) {
		reply.ok();
		//reply.error(ENOSYS);
	}

	fn releasedir(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _flags: i32, reply: fuser::ReplyEmpty) {
		reply.ok();
	}

	fn fsyncdir(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _datasync: bool, reply: fuser::ReplyEmpty) {
		reply.error(ENOSYS);
	}

	fn statfs(&mut self, _req: &Request<'_>, _ino: u64, reply: fuser::ReplyStatfs) {
		reply.statfs(0, 0, 0, 0, 0, 512, 255, 0);
	}

	//setxattr is called to set an xattr on a node.
	fn setxattr(&mut self, _req: &Request<'_>, _ino: u64, _name: &std::ffi::OsStr, _value: &[u8], _flags: i32, _position: u32, reply: fuser::ReplyEmpty) {
		let inode_idx = _ino as usize;

		//sanity check that the inode passed to us actually exists
		if inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//get a [mut] ref to the node
		let node = &mut self.nodes[inode_idx];

		/*
			put the value we were given into the xattrs with the given name, overwriting any previous value.
			Per https://www.man7.org/linux/man-pages/man7/xattr.7.html:
				"Extended attributes are accessed as atomic objects.  Reading
				(getxattr(2)) retrieves the whole value of an attribute and
				stores it in a buffer.  Writing (setxattr(2)) replaces any
				previous value with the new value."
		*/
		node.xattrs.insert(_name.to_os_string(), Vec::from(_value));
		reply.ok();
	}

	fn getxattr(&mut self, _req: &Request<'_>, _ino: u64, _name: &std::ffi::OsStr, _size: u32, reply: fuser::ReplyXattr) {
		let inode_idx = _ino as usize;

		//sanity check that the inode passed to us actually exists
		if inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//get a [mut] ref to the node
		let node = &mut self.nodes[inode_idx];

		match node.xattrs.get(_name) {
			Some(val) => {
				//If size is 0, the size of the value should be sent with reply.size()
				if _size == 0 {
					reply.size(val.len() as u32);
				} else if val.len() <= _size as usize {
					////If size is not 0, and the value fits, send it with reply.data()...
					reply.data(val.as_slice());
				} else {
					//... or reply.error(ERANGE) if it doesn’t.
					reply.error(ERANGE);
				}
			}
			None => reply.error(ENODATA),
		}
	}

	//listxattr is called to list xattr names.
	//ref https://www.man7.org/linux/man-pages/man2/listxattr.2.html
	fn listxattr(&mut self, _req: &Request<'_>, _ino: u64, _size: u32, reply: fuser::ReplyXattr) {
		let inode_idx = _ino as usize;

		//sanity check that the inode passed to us actually exists
		if inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//get a [mut] ref to the node
		let node = &mut self.nodes[inode_idx];

		/*
		from https://www.man7.org/linux/man-pages/man2/listxattr.2.html
			"The list of names is returned as an unordered array of null-
			terminated character strings (attribute names are separated by
			null bytes ('\0'))"

		This is the reason that:
				A) when returning the length of the attribute names list, we add 1 each time we add the length of a key.
				   (1 extra len for the null byte)
				B) When returning the actual attribute names, we add a null byte (\0) after appending each item.
		*/

		//If size is 0, the size of the value should be sent with reply.size()
		if _size == 0 {
			let size = node.xattrs.keys().fold(0, |accumulator, k| accumulator + k.as_bytes().len() + 1) as u32;
			reply.size(size);
		} else if node.xattrs.len() <= _size as usize {
			////If size is not 0, and the value fits, send it with reply.data()...
			let data = node.xattrs.keys().fold(vec![], |mut accumulator, k| {
				accumulator.extend_from_slice(k.as_bytes());
				accumulator.push('\0' as u8);
				accumulator
			});
			reply.data(data.as_slice());
		} else {
			//... or reply.error(ERANGE) if it doesn’t.
			reply.error(ERANGE);
		}
	}

	//removexattr removes an extended attribute
	//https://www.man7.org/linux/man-pages/man2/removexattr.2.html
	fn removexattr(&mut self, _req: &Request<'_>, _ino: u64, _name: &std::ffi::OsStr, reply: fuser::ReplyEmpty) {
		let inode_idx = _ino as usize;

		//sanity check that the inode passed to us actually exists
		if inode_idx >= self.nodes.len() {
			reply.error(ENOENT);
			return;
		}

		//get a [mut] ref to the node
		let node = &mut self.nodes[inode_idx];

		if let Some(_) = node.xattrs.remove(_name) {
			reply.ok();
		} else {
			reply.error(ENODATA);
		}
	}

	fn access(&mut self, _req: &Request<'_>, _ino: u64, _mask: i32, reply: fuser::ReplyEmpty) {
		reply.error(ENOSYS);
	}

	fn create(&mut self, _req: &Request<'_>, _parent: u64, _name: &std::ffi::OsStr, _mode: u32, _umask: u32, _flags: i32, reply: fuser::ReplyCreate) {
		reply.error(ENOSYS);
	}

	fn getlk(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _lock_owner: u64, _start: u64, _end: u64, _typ: i32, _pid: u32, reply: fuser::ReplyLock) {
		reply.error(ENOSYS);
	}

	fn setlk(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _lock_owner: u64, _start: u64, _end: u64, _typ: i32, _pid: u32, _sleep: bool, reply: fuser::ReplyEmpty) {
		reply.error(ENOSYS);
	}

	fn bmap(&mut self, _req: &Request<'_>, _ino: u64, _blocksize: u32, _idx: u64, reply: fuser::ReplyBmap) {
		reply.error(ENOSYS);
	}

	fn ioctl(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _flags: u32, _cmd: u32, _in_data: &[u8], _out_size: u32, reply: fuser::ReplyIoctl) {
		reply.error(ENOSYS);
	}

	fn fallocate(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _offset: i64, _length: i64, _mode: i32, reply: fuser::ReplyEmpty) {
		reply.error(ENOSYS);
	}

	fn lseek(&mut self, _req: &Request<'_>, _ino: u64, _fh: u64, _offset: i64, _whence: i32, reply: fuser::ReplyLseek) {
		reply.error(ENOSYS);
	}

	fn copy_file_range(
		&mut self, _req: &Request<'_>, _ino_in: u64, _fh_in: u64, _offset_in: i64, _ino_out: u64, _fh_out: u64, _offset_out: i64, _len: u64, _flags: u32,
		reply: fuser::ReplyWrite,
	) {
		reply.error(ENOSYS);
	}
}
