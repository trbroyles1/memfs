# MemFS - a FUSE filesystem in Rust

## About

MemFS is a toy implementation of FUSE in Rust. My only real goals for it are:

1. continue improving my competencies in Rust
2. Learn more about FUSE from a developer perspective
3. Learn more about considerations that have to be taken into account when implementing a filesystem

The entire filesystem is implemented in memory (hence the name). When you unmount it, everything on it is lost. I did say it was a toy implementation, right ?

The implementation of the filesystem itself is done on the `MemFs` struct. Each inode of the filesystem is a `MemFsNode` in the `nodes: Vec<MemFsNode>` attribute of the `MemFs` struct.

The attributes of a `MemFsNode` along with a plain-english description of each:


| attribute | what it is                                                                                          |
| ----------- | ----------------------------------------------------------------------------------------------------- |
| name      | The name of the file/directory                                                                      |
| attrs     | The attributes of the file/directory (owner, permissions, etc)the inode                             |
| parent    | the inode number of the parent (directory, presumably)                                              |
| children  | a list of inode numbers of children (files or directories)                                          |
| lookup    | a lookup table translating names if children into their inode numbers                               |
| data      | an optional array (err, slice...) of`u8` containing the data of the node (only applicable to files) |

Stuff to do, and its status so far:

* [X] Creating files
* [X] Creating directories
* [X] Reading file attributes
* [X] Setting file attributes
* [X] stat files
* [X] Writing to files
* [X] Reading from files
* [X] Renaming / moving files
* [X] Removing files
* [X] Removing directories
* [ ] Handling ownership & permissions
* [X] Handling xattrs
* [ ] Organize the code better so it isn't one giant file.
* [ ] Other things I doubtless haven't thought about yet

Example directory traversal with some toy files:

```
[ryan@aaaartix memfs]$ tree .
.
├── bar
│   ├── bar_1.txt
│   ├── bar_2.txt
│   ├── bar_3.txt
│   └── baz
│       ├── baz_1.txt
│       ├── baz_2.txt
│       └── baz_3.txt
└── foo
    ├── foo_1.txt
    ├── foo_2.txt
    └── foo_3.txt

3 directories, 9 files
[ryan@aaaartix memfs]$ cd bar/baz
[ryan@aaaartix baz]$ echo "some demo text" > demo.txt
[ryan@aaaartix baz]$ ls -l
total 1
-rwxrwxrwx 0 ryan ryan  0 Oct 10 22:17 baz_1.txt
-rwxrwxrwx 0 ryan ryan  0 Oct 10 22:17 baz_2.txt
-rwxrwxrwx 0 ryan ryan  0 Oct 10 22:17 baz_3.txt
-rwxrwxrwx 0 ryan ryan 15 Oct 10 22:18 demo.txt
[ryan@aaaartix baz]$ cat demo.txt
some demo text
[ryan@aaaartix baz]$
```

Write performance with a random ubuntu ISO I had laying around:

```
[ryan@aaaartix memfs]$ rsync --progress ~/Downloads/ubuntu-21.04-desktop-amd64.iso .
ubuntu-21.04-desktop-amd64.iso
  2,818,738,176 100%  324.22MB/s    0:00:08 (xfr#1, to-chk=0/1)
[ryan@aaaartix memfs]$
```

Stat of the same ISO after moving:

```
[ryan@aaaartix memfs]$ stat ubuntu-21.04-desktop-amd64.iso
  File: ubuntu-21.04-desktop-amd64.iso
  Size: 2818738176      Blocks: 2820       IO Block: 4096   regular file
Device: 0,120   Inode: 15          Links: 0
Access: (0777/-rwxrwxrwx)  Uid: ( 1000/    ryan)   Gid: ( 1000/    ryan)
Access: 2021-10-10 22:20:58.957255492 -0400
Modify: 2021-10-10 22:20:58.957255492 -0400
Change: 2021-10-10 22:20:58.957255492 -0400
 Birth: -
[ryan@aaaartix memfs]$
```

## Running it

1. Download it via zip or clone or something... you know the deal
2. make /some/mount/point
3. From the project dir: `cargo run /some/mount/point`
4. Have fun

## Crates Used

* `log`
* `pretty_env_logger`
* `fuser`
* `time`
* `libc`
* `clap`

## License, contributing, etc

As far as license: it's a toy project. Do whatever you want with it. But don't run it in production. And if you do, don't blame me when you lose data.

As far as contributing: it's a personal toy project so I probably won't take pull requests on it, at least for now. Kinda want to learn on my own steam. That said, I'm all for having advice and suggestions so that I can learn better. So feel free to shoot me any suggestions / ideas you have. I'm happy to learn.
